<?php

namespace Pilyavskiy\School\Events;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Pilyavskiy\School\Contracts\NewsInterface;

class NewsUpdated implements ShouldQueue
{
    use Queueable, SerializesModels;

    /** @var NewsInterface $news */
    public $news;

    public function __construct(NewsInterface $news)
    {
        $this->news = $news;
    }

}
