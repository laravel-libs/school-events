<?php

namespace Pilyavskiy\School\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Pilyavskiy\School\Contracts\NewsInterface;

class NewsDTO implements NewsInterface, Arrayable
{
    private $id = 0;
    private $title = '';
    private $slug = '';
    private $image = '';
    private $text = '';
    private $createdAt = '';
    private $updatedAt = '';

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): NewsInterface
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): NewsInterface
    {
        $this->title = $title;

        return $this;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): NewsInterface
    {
        $this->image = $image;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): NewsInterface
    {
        $this->slug = $slug;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): NewsInterface
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function setCreatedAt(string $createdAt): NewsInterface
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): string
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(string $updatedAt): NewsInterface
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function toArray(): array
    {
        return [
            'id'        => $this->id,
            'title'     => $this->title,
            'slug'      => $this->slug,
            'image'     => $this->image,
            'text'      => $this->text,
            'createdAt' => $this->createdAt,
            'updatedAt' => $this->updatedAt,
        ];
    }
}
