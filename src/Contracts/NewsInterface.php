<?php

namespace Pilyavskiy\School\Contracts;

interface NewsInterface
{
    public function getId(): int;
    public function setId(int $id): self;
    public function getTitle(): string;
    public function setTitle(string $title): self;
    public function getImage(): string;
    public function setImage(string $image): self;
    public function getSlug(): string;
    public function setSlug(string $slug): self;
    public function getText(): string;
    public function setText(string $text): self;
    public function getCreatedAt(): string;
    public function setCreatedAt(string $createdAt): self;
    public function getUpdatedAt(): string;
    public function setUpdatedAt(string $updatedAt): self;
}
